package src;

import javax.swing.*;

public class Case extends JPanel {
    public static int idCount = 1;
    private int id, x, y;
    private State state;
    private Grille grille;

    private JLabel label;

    public Case(int x, int y, State state, Grille grille) {
        this.id = idCount;
        this.x = x;
        this.y = y;
        this.state = state;
        this.grille = grille;
        label = new JLabel(new ImageIcon("image/" + state.toString().toLowerCase() + ".png"));
        add(label);
        idCount++;
    }

    public int getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public State getState() {
        return state;
    }

    public Grille getGrille() {
        return grille;
    }

    public JLabel getLabel() {
        return label;
    }

    public void setState(State state) {
        this.state = state;
        label.setIcon(new ImageIcon("image/" + state.toString().toLowerCase() + ".png"));
    }

    @Override
    public String toString() {
        return "id = " + id + " (" + x + ", " + y + ") : " + state;
    }
}