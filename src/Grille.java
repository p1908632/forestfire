package src;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Grille {
    private int hauteur, largeur;
    private int nbTileBurning = 0, nbTileBurnt = 0, nbSteps = 0;
    private double probability;
    private Case[] listeCase;

    private JFrame frame;
    public Grille(String file) {
        try {
            frame = new JFrame("Simulation");
            List<String> strfile = Files.readAllLines((Paths.get(file)));
            this.hauteur = Integer.parseInt(strfile.get(0).split("=")[1]);
            this.largeur = Integer.parseInt(strfile.get(1).split("=")[1]);
            this.probability = Double.parseDouble(strfile.get(2).split("=")[1]);
            listeCase = new Case[hauteur * largeur];
            for (int h = 0; h < hauteur; h++) {
                for (int l = 0; l < largeur; l++) {
                    Case carreau = new Case(h, l, State.INITIAL, this);
                    listeCase[carreau.getId() - 1] = carreau;
                    frame.add(carreau.getLabel());
                }
            }
            for (int i = 4; i < strfile.toArray().length; i++) {
                int h = Integer.parseInt(strfile.get(i).split(",")[0]);
                int l = Integer.parseInt(strfile.get(i).split(",")[1]);
                Case tile = getCase(h, l);
                tile.setState(State.BURNING);
                nbTileBurning++;
            }
            frame.setLayout(new GridLayout(hauteur,largeur));
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public int getHauteur() {
        return hauteur;
    }

    public int getLargeur() {
        return largeur;
    }

    public int getNbTileBurning() {
        return nbTileBurning;
    }

    public int getNbTileBurnt() {
        return nbTileBurnt;
    }

    public int getNbSteps() {
        return nbSteps;
    }

    public double getProbability() {
        return probability;
    }

    public Case[] getListeCase() {
        return listeCase;
    }

    public Case getCase(int h, int l) {
        for (Case tile : listeCase) {
            if ((h == tile.getX()) && (l == tile.getY())) return tile;
        }
        return null;
    }

    public JFrame getFrame() {
        return frame;
    }

    public Case[] getAdjacentTiles(Case tile) {
        ArrayList<Case> liste = new ArrayList<Case>();
        int[] h = {tile.getX() - 1, tile.getX() + 1};
        int[] l = {tile.getY() - 1, tile.getY() + 1};
        Case carreau = null;
        for (int i : h) {
            carreau = getCase(i, tile.getY());
            if (carreau != null) liste.add(carreau);
        }
        for (int i : l) {
            carreau = getCase(tile.getX(), i);
            if (carreau != null) liste.add(carreau);
        }
        return liste.toArray(new Case[0]);
    }

    public boolean run() {
        nbSteps++;
        Random rand = new Random();
        ArrayList<Case> list = new ArrayList<Case>();
        for (Case tile : listeCase) {
            if ((!list.contains(tile)) && tile.getState() == State.BURNING) {
                tile.setState(State._BURNT_);
                nbTileBurning--;
                nbTileBurnt++;
                for (Case carreau : getAdjacentTiles(tile)) {
                    if (carreau.getState() == State.INITIAL) {
                        if (rand.nextDouble() <= probability) {
                            carreau.setState(State.BURNING);
                            list.add(carreau);
                            nbTileBurning++;
                        }
                    }
                }
            }
        }
        boolean bool = nbTileBurning > 0;
        if(bool) return true;
        else {
            JOptionPane.showMessageDialog(null, "Nombre de cases réduites en cendre : " + nbTileBurnt + "\n" +
                    "Nombre d’étapes écoulées à la fin de la simulation : " + nbSteps);
            return false;
        }
    }

    @Override
    public String toString() {
        String retour = "";
        for (int i = 0; i < hauteur; i++) {
            retour += "| ";
            for (int j = 0; j < largeur; j++) {
                Case tile = getCase(i, j);
                retour += tile.getState() + " ";
            }
            retour += "|\n";
        }
        return retour;
    }
}