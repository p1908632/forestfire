package src;
import java.lang.Thread;
public class Main {
    public static void main(String[] args) throws InterruptedException {

        Grille grille = new Grille("files/configuration.txt");
        int time = 1000;
        System.out.println(grille);
        Thread.sleep(time);
        while (grille.run()) {
            System.out.println(grille);
            Thread.sleep(time);
        }
        System.out.println(grille);

    }
}
